/*
 * Copyright (c) 2019 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef ORGAN_H
#define ORGAN_H

#define NDRAWBARS	9

struct manualcfg {
	int	keys_channel;
	struct {
		int	channel;
		int	controller;
	} drawbars[2][NDRAWBARS], vibrato, chorus;
};

struct midicfg {
	struct midicfg	*next;

	const char	*name;
	struct manualcfg great;
	struct manualcfg swell;

	jack_port_t	*port;
};

struct preset_bank {
	uint8_t	swell[10][NDRAWBARS];
	uint8_t	great[10][NDRAWBARS];
};

/* organ.c */
extern struct midicfg *midicfgs;
extern const struct preset_bank *presets;

/* presets.c */
extern const struct preset_bank liturgical_presets;
extern const struct preset_bank jazz_presets;
extern const struct preset_bank theatre_presets;

/* parse.y */
void parse_config(const char *);

#endif /* !ORGAN_H */
