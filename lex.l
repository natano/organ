/*
 * Copyright (c) 2019 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

%{
#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "parse.h"
%}

%option never-interactive
%option yylineno
%option noyywrap
%option nounput

%x COMMENT

%%

midi		return MIDI;
great		return GREAT;
swell		return SWELL;
keys		return KEYS;
drawbar		return DRAWBAR;
channel		return CHANNEL;
controller	return CONTROLLER;
vibrato		return VIBRATO;
chorus		return CHORUS;
presets		return PRESETS;
liturgical	return LITURGICAL;
jazz		return JAZZ;
theatre		return THEATRE;

1st	{
		yylval.num = 0;
		return DGROUP;
	}

2nd	{
		yylval.num = 1;
		return DGROUP;
	}

[0-9]+	{
		yylval.num = atoi(yytext);
		return NUMBER;
	}

\"[^"]*\"	{
		yylval.str = strndup(yytext + 1, strlen(yytext) - 2);
		if (yylval.str == NULL)
			err(1, NULL);
		return STR;
	}

"/*"		BEGIN(COMMENT);
<COMMENT>"*/"	BEGIN(INITIAL);
<COMMENT>\n|.	/* ignore */

[ \t]+		/* ignore whitespace */

.|\n		return yytext[0];

%%
