/*
 * Copyright (c) 2019 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <err.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <jack/jack.h>
#include <jack/midiport.h>

#include "organ.h"

#define VIB_FREQ	7
#define NWHEELS		91
#define NKEYS		61

typedef jack_default_audio_sample_t sample_t;

struct manual {
	uint64_t	 keys;
	uint8_t		 drawbars[2][NDRAWBARS];
	const uint8_t	*preset;
	int		 vibrato, chorus;
	uint64_t	 tick;
	sample_t	*vbuf;
};

static void midi_message(struct midicfg *, uint8_t *);
static void midi_note(struct midicfg *, uint8_t, uint8_t, uint8_t);
static void midi_ctl(struct midicfg *, uint8_t, uint8_t, uint8_t);
static int process(jack_nframes_t, void *);
static int set_sample_rate(jack_nframes_t, void *);
static void server_shutdown(void *);
static void snd_out_manual(sample_t *, unsigned int, struct manual *);
static sample_t vibrato_box(sample_t, struct manual *);
static void handle_sig(int);
static void usage(void);

extern const char *__progname;

struct midicfg *midicfgs = NULL;
const struct preset_bank *presets = &liturgical_presets;

static const double wheels[NWHEELS] = {
	32.692, 34.634, 36.712, 38.889, 41.200, 43.636, 46.250, 49.000, 51.892,
	55.000, 58.261, 61.714, 65.385, 69.268, 73.425, 77.778, 82.400, 87.273,
	92.500, 98.000, 103.784, 110.000, 116.522, 123.429, 130.769, 138.537,
	146.849, 155.556, 164.800, 174.545, 185.000, 196.000, 207.568, 220.000,
	233.043, 246.857, 261.538, 277.073, 293.699, 311.111, 329.600, 349.091,
	370.000, 392.000, 415.135, 440.000, 466.087, 493.714, 523.077, 554.146,
	587.397, 622.222, 659.200, 698.182, 740.000, 784.000, 830.270, 880.000,
	932.174, 987.429, 1046.154, 1108.293, 1174.795, 1244.444, 1318.400,
	1396.364, 1480.000, 1568.000, 1660.541, 1760.000, 1864.348, 1974.857,
	2092.308, 2216.585, 2349.589, 2488.889, 2636.800, 2792.727, 2960.000,
	3136.000, 3321.081, 3520.000, 3728.696, 3949.714, 4189.091, 4440.000,
	4704.000, 4981.622, 5280.000, 5593.043, 5924.571,
};

/*
 * Wiring information: key # -> tone wheel
 * Off by one, because that's the format usually used in wiring diagrams
 * obtainable from the internet.
 */
static const int key_wiring[NKEYS][NDRAWBARS] = {
	{ 13, 20, 13, 25, 32, 37, 41, 44, 49 },
	{ 14, 21, 14, 26, 33, 38, 42, 45, 50 },
	{ 15, 22, 15, 27, 34, 39, 43, 46, 51 },
	{ 16, 23, 16, 28, 35, 40, 44, 47, 52 },
	{ 17, 24, 17, 29, 36, 41, 45, 48, 53 },
	{ 18, 25, 18, 30, 37, 42, 46, 49, 54 },
	{ 19, 26, 19, 31, 38, 43, 47, 50, 55 },
	{ 20, 27, 20, 32, 39, 44, 48, 51, 56 },
	{ 21, 28, 21, 33, 40, 45, 49, 52, 57 },
	{ 22, 29, 22, 34, 41, 46, 50, 53, 58 },
	{ 23, 30, 23, 35, 42, 47, 51, 54, 59 },
	{ 24, 31, 24, 36, 43, 48, 52, 55, 60 },
	{ 13, 32, 25, 37, 44, 49, 53, 56, 61 },
	{ 14, 33, 26, 38, 45, 50, 54, 57, 62 },
	{ 15, 34, 27, 39, 46, 51, 55, 58, 63 },
	{ 16, 35, 28, 40, 47, 52, 56, 59, 64 },
	{ 17, 36, 29, 41, 48, 53, 57, 60, 65 },
	{ 18, 37, 30, 42, 49, 54, 58, 61, 66 },
	{ 19, 38, 31, 43, 50, 55, 59, 62, 67 },
	{ 20, 39, 32, 44, 51, 56, 60, 63, 68 },
	{ 21, 40, 33, 45, 52, 57, 61, 64, 69 },
	{ 22, 41, 34, 46, 53, 58, 62, 65, 70 },
	{ 23, 42, 35, 47, 54, 59, 63, 66, 71 },
	{ 24, 43, 36, 48, 55, 60, 64, 67, 72 },
	{ 25, 44, 37, 49, 56, 61, 65, 68, 73 },
	{ 26, 45, 38, 50, 57, 62, 66, 69, 74 },
	{ 27, 46, 39, 51, 58, 63, 67, 70, 75 },
	{ 28, 47, 40, 52, 59, 64, 68, 71, 76 },
	{ 29, 48, 41, 53, 60, 65, 69, 72, 77 },
	{ 30, 49, 42, 54, 61, 66, 70, 73, 78 },
	{ 31, 50, 43, 55, 62, 67, 71, 74, 79 },
	{ 32, 51, 44, 56, 63, 68, 72, 75, 80 },
	{ 33, 52, 45, 57, 64, 69, 73, 76, 81 },
	{ 34, 53, 46, 58, 65, 70, 74, 77, 82 },
	{ 35, 54, 47, 59, 66, 71, 75, 78, 83 },
	{ 36, 55, 48, 60, 67, 72, 76, 79, 84 },
	{ 37, 56, 49, 61, 68, 73, 77, 80, 85 },
	{ 38, 57, 50, 62, 69, 74, 78, 81, 86 },
	{ 39, 58, 51, 63, 70, 75, 79, 82, 87 },
	{ 40, 59, 52, 64, 71, 76, 80, 83, 88 },
	{ 41, 60, 53, 65, 72, 77, 81, 84, 89 },
	{ 42, 61, 54, 66, 73, 78, 82, 85, 90 },
	{ 43, 62, 55, 67, 74, 79, 83, 86, 91 },
	{ 44, 63, 56, 68, 75, 80, 84, 87, 80 },
	{ 45, 64, 57, 69, 76, 81, 85, 88, 81 },
	{ 46, 65, 58, 70, 77, 82, 86, 89, 82 },
	{ 47, 66, 59, 71, 78, 83, 87, 90, 83 },
	{ 48, 67, 60, 72, 79, 84, 88, 91, 84 },
	{ 49, 68, 61, 73, 80, 85, 89, 80, 85 },
	{ 50, 69, 62, 74, 81, 86, 90, 81, 86 },
	{ 51, 70, 63, 75, 82, 87, 91, 82, 87 },
	{ 52, 71, 64, 76, 83, 88, 80, 83, 88 },
	{ 53, 72, 65, 77, 84, 89, 81, 84, 89 },
	{ 54, 73, 66, 78, 85, 90, 82, 85, 90 },
	{ 55, 74, 67, 79, 86, 91, 83, 86, 91 },
	{ 56, 75, 68, 80, 87, 80, 84, 87, 80 },
	{ 57, 76, 69, 81, 88, 81, 85, 88, 81 },
	{ 58, 77, 70, 82, 89, 82, 86, 89, 82 },
	{ 59, 78, 71, 83, 90, 83, 87, 90, 83 },
	{ 60, 79, 72, 84, 91, 84, 88, 91, 84 },
	{ 61, 80, 73, 85, 80, 85, 89, 80, 85 },
};

struct manual great, swell;

static jack_nframes_t rate;
static jack_nframes_t vib_frames;
static sample_t *sintbl;
static volatile sig_atomic_t quit;

static jack_client_t *client;
static jack_port_t *output_port;


int
main(int argc, char **argv)
{
	struct midicfg *mcp;
	const char *fname;
	int c;

	fname = NULL;

	while ((c = getopt(argc, argv, "f:")) != -1) {
		switch (c) {
		case 'f':
			fname = optarg;
			break;
		default:
			usage();
		}
	}
	argc -= optind;
	argv += optind;

	if (argc > 0)
		usage();

	if (fname == NULL)
		usage();
	parse_config(fname);

	great.preset = great.drawbars[0];
	swell.preset = swell.drawbars[0];

	client = jack_client_open("organ", JackNullOption, NULL, NULL);
	if (client == NULL)
		errx(1, "failed to connect to jack");

	printf("client name: %s\n", jack_get_client_name(client));

	jack_set_process_callback(client, process, NULL);
	jack_set_sample_rate_callback(client, set_sample_rate, NULL);
	jack_on_shutdown(client, server_shutdown, NULL);

	for (mcp = midicfgs; mcp != NULL; mcp = mcp->next) {
		mcp->port = jack_port_register(client, mcp->name,
		    JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
		if (mcp->port  == NULL)
			errx(1, "failed to create midi port");
	}

	output_port = jack_port_register(client, "out",
	    JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
	if (output_port == NULL)
		errx(1, "failed to create output port");


	set_sample_rate(jack_get_sample_rate(client), NULL);

	if (jack_activate(client))
		errx(1, "failed to activate client");

#if 0
	const char **ports;
	ports = jack_get_ports(client, NULL, NULL,
	    JackPortIsPhysical | JackPortIsInput);
	if (ports == NULL)
		errx(1, "failed to find playback port");

	if (jack_connect(client, jack_port_name(output_port), ports[0]))
		errx(1, "failed to connect playback port");

	free(ports);
# endif

	if (signal(SIGINT, handle_sig) == SIG_ERR ||
	    signal(SIGTERM, handle_sig) == SIG_ERR)
		err(1, "signal");

	while (!quit)
		sleep(1);

	jack_client_close(client);
	return 0;
}

static void
midi_message(struct midicfg *mcp, uint8_t *data)
{
	uint8_t status = data[0];

	switch (status >> 4) {
	case 0x8:
		/* Note-Off */
		midi_note(mcp, status & 0x0f, data[1], 0);
		break;
	case 0x9:
		/* Note-On */
		midi_note(mcp, status & 0x0f, data[1], data[2]);
		break;
	case 0xb:
		/* Control Change */
		midi_ctl(mcp, status & 0x0f, data[1], data[2]);
		break;
	}
}

static void
midi_note(struct midicfg *mcp, uint8_t channel, uint8_t note, uint8_t velocity)
{
	uint8_t key;

	printf("NOTE channel=%d note=%d velocity=%d\n", channel + 1, note + 1,
	    (int)velocity);

	if (channel == mcp->swell.keys_channel) {
		if (note >= 24 && note <= 33 && velocity > 0) {
			swell.preset = presets->swell[note - 24];
		} else if (note >= 34 && note <= 35 && velocity > 0) {
			swell.preset = swell.drawbars[note - 34];
		} else if (note >= 36 && note <= 96) {
			key = note - 36;
			if (velocity > 0)
				swell.keys |= (uint64_t)1 << key;
			else
				swell.keys &= ~((uint64_t)1 << key);
		}
	}
	if (channel == mcp->great.keys_channel) {
		if (note >= 24 && note <= 33 && velocity > 0) {
			great.preset = presets->great[note - 24];
		} else if (note >= 34 && note <= 35 && velocity > 0) {
			great.preset = great.drawbars[note - 34];
		} else if (note >= 36 && note <= 96) {
			key = note - 36;
			if (velocity > 0)
				great.keys |= (uint64_t)1 << key;
			else
				great.keys &= ~((uint64_t)1 << key);
		}
	}
}

static void
midi_ctl(struct midicfg *mcp, uint8_t channel, uint8_t controller, uint8_t val)
{
	int i, j;

	printf("CTL channel=%d controller=%d val=%d\n", channel + 1,
	    controller + 1, (int)val);

	for (i = 0; i < 2; i++) {
		for (j = 0; j < NDRAWBARS; j++) {
			if (channel == mcp->great.drawbars[i][j].channel &&
			    controller == mcp->great.drawbars[i][j].controller)
				great.drawbars[i][j] = val;
			if (channel == mcp->swell.drawbars[i][j].channel &&
			    controller == mcp->swell.drawbars[i][j].controller)
				swell.drawbars[i][j] = val;
		}
	}
	if (channel == mcp->great.vibrato.channel &&
	    controller == mcp->great.vibrato.controller)
		great.vibrato = (val > 0);
	if (channel == mcp->swell.vibrato.channel &&
	    controller == mcp->swell.vibrato.controller)
		swell.vibrato = (val > 0);
	if (channel == mcp->great.chorus.channel &&
	    controller == mcp->great.chorus.controller)
		great.chorus = (val > 0);
	if (channel == mcp->swell.chorus.channel &&
	    controller == mcp->swell.chorus.controller)
		swell.chorus = (val > 0);
}

static int
process(jack_nframes_t nframes, void *arg)
{
	struct midicfg *mcp;
	void *midi_buffer;
	jack_nframes_t ev_count, i;
	jack_midi_event_t ev;
	sample_t *buf;

	(void)arg;	/* unused */

	for (mcp = midicfgs; mcp != NULL; mcp = mcp->next) {
		midi_buffer = jack_port_get_buffer(mcp->port, nframes);
		ev_count = jack_midi_get_event_count(midi_buffer);

		for (i = 0; i < ev_count; i++) {
			if (jack_midi_event_get(&ev, midi_buffer, i))
				continue;

			midi_message(mcp, ev.buffer);
		}
	}

	buf = jack_port_get_buffer(output_port, nframes);

	memset(buf, 0, nframes * sizeof(sample_t));
	snd_out_manual(buf, nframes, &great);
	snd_out_manual(buf, nframes, &swell);

	return 0;
}

static int
set_sample_rate(jack_nframes_t nframes, void *arg)
{
	jack_nframes_t i;

	(void)arg;	/* unused */

	rate = nframes;
	vib_frames = rate / 1000;	/* 1 ms */

	/* (re)alloc and initialize sine lookup table. */
	sintbl = reallocarray(sintbl, rate, sizeof(sample_t));
	if (sintbl == NULL)
		err(1, NULL);

	for (i = 0; i < rate; i++)
		sintbl[i] = sinf(i * M_PI * 2 / rate);

	great.vbuf = reallocarray(great.vbuf, vib_frames, sizeof(sample_t));
	swell.vbuf = reallocarray(swell.vbuf, vib_frames, sizeof(sample_t));
	if (great.vbuf == NULL || swell.vbuf == NULL)
		err(1, NULL);

	return 0;
}

static void
server_shutdown(void *arg)
{
	(void)arg;	/* unused */

	fprintf(stderr, "jack server shut down\n");
	quit = 1;
}

static void
snd_out_manual(sample_t *buf, unsigned int nframes, struct manual *mp)
{
	sample_t sample;
	unsigned int key, wheel, phase, i;
	int wheel_vol[NWHEELS] = { 0 };

	for (key = 0; key < NKEYS; key++) {
		if ((mp->keys & ((uint64_t)1 << key)) == 0)
			continue;

		for (i = 0; i < NDRAWBARS; i++)
			wheel_vol[key_wiring[key][i] - 1] += mp->preset[i];
	}

	for (i = 0; i < nframes; i++, mp->tick++) {
		sample = 0;

		for (wheel = 0; wheel < NWHEELS; wheel++) {
			if (wheel_vol[wheel] == 0)
				continue;

			phase = fmod(mp->tick, rate / wheels[wheel]) * wheels[wheel];
			sample += sintbl[phase] * wheel_vol[wheel] / 127;
		}

		sample /= 14;	/* XXX */
		if (sample < -1. || sample > 1.)
			fprintf(stderr, "xflow: %f\n", sample);

		buf[i] += vibrato_box(sample, mp);
	}
}

static sample_t
vibrato_box(sample_t dry, struct manual *mp)
{
	sample_t wet;
	float phase, c;
	int a, b;

	mp->vbuf[mp->tick % vib_frames] = dry;

	if (!mp->vibrato)
		return dry;

	phase = (sintbl[mp->tick % (rate / VIB_FREQ) * VIB_FREQ] + 1.) / 2.;
	phase *= vib_frames - 1;

	a = floorf(phase);
	b = ceilf(phase);
	c = phase - a;

	a = (mp->tick - a) % vib_frames;
	b = (mp->tick - b) % vib_frames;

	wet = (mp->vbuf[a] * (1. - c) + mp->vbuf[b] * c);
	if (mp->chorus)
		wet = (wet + dry) / 2;

	return wet;
}

static void
handle_sig(int signum)
{
	(void)signum;	/* unused */

	quit = 1;
}

static void
usage(void)
{
	fprintf(stderr, "usage: %s -f file\n", __progname);
	exit(1);
}
