/*
 * Copyright (c) 2019 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stddef.h>
#include <stdint.h>

#include <jack/jack.h>

#include "organ.h"

#define P(a, b, c, d, e, f, g, h, i)	{			\
	a * 127 / 8, b * 127 / 8,				\
	c * 127 / 8, d * 127 / 8, e * 127 / 8, f * 127 / 8,	\
	g * 127 / 8, h * 127 / 8, i * 127 / 8,			\
}

/*
 * Liturgical Bank
 */
const struct preset_bank liturgical_presets = {
	.swell = {
		P(0,0, 0,0,0,0, 0,0,0),	/* C  - Cancel */
		P(0,0, 5,3,2,0, 0,0,0),	/* C# - Stopped Flute (pp) */
		P(0,0, 4,4,3,2, 0,0,0),	/* D  - Dulciana (ppp) */
		P(0,0, 8,7,4,0, 0,0,0),	/* D# - French Horn (mf) */
		P(0,0, 4,5,4,4, 2,2,2),	/* E  - Salicional (pp) */
		P(0,0, 5,4,0,3, 0,0,0),	/* F  - Flutes 8' & 4' (p) */
		P(0,0, 4,6,7,5, 3,0,0),	/* F# - Oboe Horn (mf) */
		P(0,0, 5,6,4,4, 3,2,0),	/* G  - Swell Diapason (mf) */
		P(0,0, 6,8,7,6, 5,4,0),	/* G# - Trumpet (f) */
		P(3,2, 7,6,4,5, 2,2,2),	/* A  - Full Swell (ff) */
					/* A# - Drawbars in 1st group */
					/* B  - Drawbars in 2nd group */
	},
	.great = {
		P(0,0, 0,0,0,0, 0,0,0),	/* C  - Cancel */
		P(0,0, 4,5,4,5, 4,4,0),	/* C# - Cello (mp) */
		P(0,0, 4,4,2,3, 2,2,0),	/* D  - Flute & String (mp) */
		P(0,0, 7,3,7,3, 4,3,0),	/* D# - Clarinet (mf) */
		P(0,0, 4,5,4,4, 2,2,0),	/* E  - Diapason, Gamba and Flute (mf) */
		P(0,0, 6,6,4,4, 3,2,2),	/* F  - Great, no reeds (f) */
		P(0,0, 5,6,4,2, 2,0,0),	/* F# - Open Diapason (f) */
		P(0,0, 6,8,4,5, 4,3,3),	/* G  - Full Great (ff) */
		P(0,0, 8,0,3,0, 0,0,0),	/* G# - Tibia Clausa (f) */
		P(4,2, 7,8,6,6, 2,4,4),	/* A  - Full Great with 16' (fff) */
					/* A# - Drawbars in 1st group */
					/* B  - Drawbars in 2nd group */
	},
};

/*
 * Jazz Bank
 */
const struct preset_bank jazz_presets = {
	.swell = {
		P(0,0, 0,0,0,0, 0,0,0),	/* C  - Cancel */
		P(8,8, 8,0,0,0, 0,0,0),	/* C# - Jimmy1 */
		P(8,8, 8,8,0,0, 0,0,0),	/* D  - Power */
		P(8,8, 8,8,0,0, 0,0,8),	/* D# - Jimmy2 */
		P(8,0, 0,0,0,8, 8,8,8),	/* E  - Squabble */
		P(8,0, 0,8,0,0, 0,0,0),	/* F  - Walter */
		P(8,8, 8,0,0,0, 0,0,8),	/* F# - Groove */
		P(8,8, 8,8,8,0, 0,0,0),	/* G  - Pop */
		P(8,8, 0,8,8,8, 0,8,0),	/* G# - Jackie */
		P(8,8, 8,8,8,8, 8,8,8),	/* A  - All Nine */
					/* A# - Drawbars in 1st group */
					/* B  - Drawbars in 2nd group */
	},
	.great = {
		P(0,0, 0,0,0,0, 0,0,0),	/* C  - Cancel */
		P(8,3, 8,0,0,0, 0,0,0),	/* C# - Jimmy 1 Bass */
		P(0,0, 8,8,0,0, 0,0,0),	/* D  - Accomp. */
		P(8,4, 8,0,0,0, 0,0,0),	/* D# - Jimmy2 Bass */
		P(8,2, 8,0,0,0, 0,0,0),	/* E  - Squabble Bass */
		P(8,0, 8,0,0,0, 0,0,0),	/* F  - Smooth Bass */
		P(8,5, 8,0,0,0, 0,0,0),	/* F# - Groove Bass */
		P(0,0, 8,8,4,0, 0,0,0),	/* G  - Pop Accomp. */
		P(8,4, 8,0,1,0, 0,0,0),	/* G# - Fat Bass */
		P(0,0, 8,8,8,6, 5,4,0),	/* A  - Full Accomp. */
					/* A# - Drawbars in 1st group */
					/* B  - Drawbars in 2nd group */
	},
};

/*
 * Theatre Bank
 */
const struct preset_bank theatre_presets = {
	.swell = {
		P(0,0, 0,0,0,0, 0,0,0),	/* C  - Cancel */
		P(0,0, 8,7,4,0, 0,0,0),	/* C# - French Horn 8' */
		P(0,0, 8,4,0,8, 0,0,4),	/* D  - Tibias 8' & 2' */
		P(0,0, 8,0,8,0, 8,4,0),	/* D# - Clarinet 8' */
		P(0,8, 8,8,0,0, 8,8,0),	/* E  - Novel Solo 8' */
		P(6,0, 8,0,8,8, 0,0,0),	/* F  - Theatre Solo 16' */
		P(0,0, 4,6,8,5, 3,0,0),	/* F# - Oboe Horn 8' */
		P(6,0, 8,8,0,7, 0,0,6),	/* G  - Full Tibias 16' */
		P(0,0, 6,8,8,8, 6,5,4),	/* G# - Trumpet 8' */
		P(7,6, 8,8,7,8, 6,6,7),	/* A  - Full Theatre Brass 16' */
					/* A# - Drawbars in 1st group */
					/* B  - Drawbars in 2nd group */

	},
	.great = {
		P(0,0, 0,0,0,0, 0,0,0),	/* C  - Cancel */
		P(0,0, 4,5,4,5, 4,4,2),	/* C# - Cello 8' */
		P(0,0, 4,4,3,2, 0,0,0),	/* D  - Dulciana 8' */
		P(0,0, 4,8,0,0, 0,0,0),	/* D# - Vibraharp 8' */
		P(0,0, 2,5,0,0, 2,3,4),	/* E  - Vox 8' & Tibia 4' */
		P(0,0, 6,5,5,4, 3,2,2),	/* F  - String Accomp. 8' */
		P(0,0, 5,6,4,2, 2,0,0),	/* F# - Open Diapason 8' */
		P(0,0, 7,6,5,6, 3,1,1),	/* G  - Full Accomp. 8' */
		P(0,0, 8,0,3,0, 0,0,0),	/* G# - Tibia 8' */
		P(8,4, 7,7,6,7, 6,6,6),	/* A  - Bombarde 16' */
					/* A# - Drawbars in 1st group */
					/* B  - Drawbars in 2nd group */
	},
};
