/*
 * Copyright (c) 2019 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

%{
#include <err.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <jack/jack.h>

#include "organ.h"

int yylex(void);

extern int yylineno;
extern FILE *yyin;

static void yyerror(const char *);
%}

%union {
	int		 num;
	char		*str;
	struct midicfg	*midi;
}

%token MIDI GREAT SWELL KEYS DRAWBAR CHANNEL CONTROLLER VIBRATO CHORUS
%token PRESETS LITURGICAL JAZZ THEATRE
%token <num> NUMBER DGROUP
%token <str> STR

%type <midi> midiblk
%type <num> channel controller

%%

top	: /* empty */
	| top '\n'
	| top midi
	| presets
	;

midi	: MIDI STR '{' midiblk '}' '\n' {
		$4->name = $2;
		$4->next = midicfgs;
		midicfgs = $4;
	}
	;

presets	: PRESETS LITURGICAL '\n' {
		presets = &liturgical_presets;
	}
	| PRESETS JAZZ {
		presets = &jazz_presets;
	}
	| PRESETS THEATRE {
		presets = &theatre_presets;
	}
	;

midiblk	: /* empty */ {
		int i, j;

		$$ = malloc(sizeof(struct midicfg));
		if ($$ == NULL)
			err(1, NULL);

		$$->great.keys_channel = -1;
		$$->great.vibrato.channel = -1;
		$$->great.vibrato.controller = -1;
		$$->great.chorus.channel = -1;
		$$->great.chorus.controller = -1;

		$$->swell.keys_channel = -1;
		$$->swell.vibrato.channel = -1;
		$$->swell.vibrato.controller = -1;
		$$->swell.chorus.channel = -1;
		$$->swell.chorus.controller = -1;

		for (i = 0; i < 2; i++) {
			for (j = 0; j < NDRAWBARS; j++) {
				$$->great.drawbars[i][j].channel = -1;
				$$->great.drawbars[i][j].controller = -1;

				$$->swell.drawbars[i][j].channel = -1;
				$$->swell.drawbars[i][j].controller = -1;
			}
		}
	}
	| midiblk '\n' {
		$$ = $1;
	}
	| midiblk GREAT KEYS channel '\n' {
		($$ = $1)->great.keys_channel = $4;
	}
	| midiblk SWELL KEYS channel '\n' {
		($$ = $1)->swell.keys_channel = $4;
	}
	| midiblk GREAT DRAWBAR DGROUP NUMBER channel controller '\n' {
		$$ = $1;
		if ($5 < 1 || $5 > NDRAWBARS)
			yyerror("invalid drawbar index");
		$$->great.drawbars[$4][$5 - 1].channel = $6;
		$$->great.drawbars[$4][$5 - 1].controller = $7;
	}
	| midiblk SWELL DRAWBAR DGROUP NUMBER channel controller '\n' {
		$$ = $1;
		if ($5 < 1 || $5 > NDRAWBARS)
			yyerror("invalid drawbar index");
		$$->swell.drawbars[$4][$5 - 1].channel = $6;
		$$->swell.drawbars[$4][$5 - 1].controller = $7;
	}
	| midiblk GREAT VIBRATO channel controller '\n' {
		$$ = $1;
		$$->great.vibrato.channel = $4;
		$$->great.vibrato.controller = $5;
	}
	| midiblk SWELL VIBRATO channel controller '\n' {
		$$ = $1;
		$$->swell.vibrato.channel = $4;
		$$->swell.vibrato.controller = $5;
	}
	| midiblk GREAT CHORUS channel controller '\n' {
		$$ = $1;
		$$->great.chorus.channel = $4;
		$$->great.chorus.controller = $5;
	}
	| midiblk SWELL CHORUS channel controller '\n' {
		$$ = $1;
		$$->swell.chorus.channel = $4;
		$$->swell.chorus.controller = $5;
	}
	;

channel	: CHANNEL NUMBER {
		if ($2 < 1 || $2 > 128)
			yyerror("invalid channel number");
		$$ = $2 - 1;
	}
	;

controller
	: CONTROLLER NUMBER {
		if ($2 < 1 || $2 > 128)
			yyerror("invalid controller number");
		$$ = $2 - 1;
	}
	;

%%

void
parse_config(const char *fname)
{
	yyin = fopen(fname, "r");
	if (yyin == NULL)
		err(1, "fopen");
	yyparse();
}

static void
yyerror(const char *s)
{
	errx(1, "%d: %s", yylineno, s);
}
